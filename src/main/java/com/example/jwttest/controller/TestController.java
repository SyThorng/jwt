package com.example.jwttest.controller;
import com.example.jwttest.model.dto.UserAppDTO;
import com.example.jwttest.model.request.UserAppRequest;
import com.example.jwttest.model.respon.Response;
import com.example.jwttest.service.UserService;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController

@SecurityRequirement(
        name = "basicAuth"
)
public class TestController {

    private  final UserService userService;

    public TestController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/welcome")
    public String welcome(){
        return "welcome";
    }
    @PostMapping("/register")
    public ResponseEntity<Response<UserAppDTO>> register(
            @RequestBody UserAppRequest userAppRequest
            ){
        Response<UserAppDTO> response=Response.<UserAppDTO>builder()
                .payload(userService.register(userAppRequest))
                .status(true)
                .message("insert Success....")
                .build();
        return ResponseEntity.ok().body(response);
    }
    @GetMapping("/user")
    public String user(){
        return "user";
    }
    @GetMapping("/admin")
    public String admin(){
        return "admin";
    }
}
