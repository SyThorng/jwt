package com.example.jwttest.repository;

import com.example.jwttest.model.UserApp;
import com.example.jwttest.model.request.UserAppRequest;
import org.apache.ibatis.annotations.*;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.List;

@Mapper
public interface UserRepository {
    @Select("""
            select name from role_tb  join user_detail ud on role_tb.id = ud.role_id 
            where user_id=#{userId}
            """)
    List<String> getRoleUser(Integer userId);


    @Select("""
            select * from user_tb where email=#{email} 
            """)
    @Results(id = "mapUser", value = {
            @Result(property = "userId", column = "id"),
            @Result(property = "roles", column = "id", many = @Many(
                    select = "getRoleUser"
            ))
    })
    UserApp findByUsername(String email);

    @Select("""
            insert into user_tb(email, password) VALUES (#{user.email},#{user.password}) returning id
            """)
    Integer insertUserApp(@Param("user") UserAppRequest userAppRequest);

    @Select("""
            insert into user_detail(user_id, role_id) VALUES (#{userID},#{roleId})
            """)
    void addUserDetail(Integer userID, Integer roleId);

    @Select("""
            Select * from user_tb where id=#{userId} 
            """)
    @ResultMap("mapUser")
    UserApp searchUserById(Integer userId);
}
