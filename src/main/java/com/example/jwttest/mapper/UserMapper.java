package com.example.jwttest.mapper;

import com.example.jwttest.model.UserApp;
import com.example.jwttest.model.dto.UserAppDTO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;


@Mapper
public interface UserMapper {

    UserMapper instance = Mappers.getMapper(UserMapper.class);

    //if field DTO khos pi feild bos UserApp
//    @Mapping(source = "userId",target = "userDTOId")
    UserAppDTO toUserAppDTO(UserApp userApp);
}
