package com.example.jwttest.service;

import com.example.jwttest.model.UserApp;
import com.example.jwttest.model.dto.UserAppDTO;
import com.example.jwttest.model.request.UserAppRequest;
import org.springframework.security.core.userdetails.UserDetailsService;

public interface UserService extends UserDetailsService {

    UserAppDTO register(UserAppRequest userAppRequest);
}
