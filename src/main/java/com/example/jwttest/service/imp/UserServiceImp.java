package com.example.jwttest.service.imp;

import com.example.jwttest.config.PasswordEncoderConfig;
import com.example.jwttest.mapper.UserMapper;
import com.example.jwttest.model.UserApp;
import com.example.jwttest.model.dto.UserAppDTO;
import com.example.jwttest.model.request.UserAppRequest;
import com.example.jwttest.repository.UserRepository;
import com.example.jwttest.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {
    private final UserRepository repository;

    private final PasswordEncoderConfig passwordEncoder;
    public UserServiceImp(UserRepository repository, PasswordEncoderConfig passwordEncoder) {
        this.repository = repository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        return repository.findByUsername(email);
    }

    @Override
    public UserAppDTO register(UserAppRequest userAppRequest) {
        userAppRequest.setPassword(passwordEncoder.passwordEncoder().encode(userAppRequest.getPassword()));
        Integer userId=repository.insertUserApp(userAppRequest);
        for (String role:userAppRequest.getRoles()
             ) {
            if(role.equals("ROLE_ADMIN")){
                repository.addUserDetail(userId,1);
            }else {
                repository.addUserDetail(userId,2);
            }
        }

        UserApp userApp=repository.searchUserById(userId);
        return UserMapper.instance.toUserAppDTO(userApp);
    }
}
