package com.example.jwttest.model.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserAppDTO {

    private Long userId;
    private String email;
    List<String> roles;
}